import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { BillSchema } from './schemas/bill.schema';
import { HotelSchema } from './schemas/hotel.schema';
import { RoomSchema } from './schemas/room.schema';
import { UserSchema } from './schemas/user.schema';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env',
      isGlobal: true,
    }),
    MongooseModule.forRoot(process.env.MONGODB_URI),
    MongooseModule.forFeature([{ name: 'user', schema: UserSchema }]),
    MongooseModule.forFeature([{ name: 'hotels', schema: HotelSchema }]),
    MongooseModule.forFeature([{ name: 'rooms', schema: RoomSchema }]),
    MongooseModule.forFeature([{ name: 'bills', schema: BillSchema }]),
  ],
  controllers: [AuthController],
  providers: [AuthService],
})
export class AuthModule {}
