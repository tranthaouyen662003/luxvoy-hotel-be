import { ApiProperty } from '@nestjs/swagger';
import { ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { ObjectId } from 'mongodb';

class Room {
  @ApiProperty()
  roomName: string;

  @ApiProperty({ type: [ObjectId] }) // Specify the type as an array of ObjectId
  roomImages: ObjectId[];

  @ApiProperty()
  type: string;

  @ApiProperty()
  price: string;

  @ApiProperty()
  status: string;

  @ApiProperty()
  capacity: string;
}

export class CreateHotelDto {
  @ApiProperty()
  image: string[];

  @ApiProperty()
  hotelName: string;

  @ApiProperty()
  address: string;

  @ApiProperty()
  rating: string;

  @ApiProperty()
  lowestPrice: string;

  @ApiProperty()
  userId: string;

  // Update the type of 'room' property to be an array of Room objects
  @ApiProperty({ type: [Room] })
  @ValidateNested({ each: true }) // Add 'each: true' to validate each element in the array
  @Type(() => Room)
  room: Room[];
}
