import { ApiProperty } from '@nestjs/swagger';

export enum RoleUser {
  USER = '0',
  ADMIN = '1',
}

export class RegisterDto {
  @ApiProperty()
  phoneNumber: string;

  @ApiProperty()
  password: string;

  @ApiProperty()
  fullName: string;

  @ApiProperty()
  role: RoleUser;

  @ApiProperty()
  street: string;

  @ApiProperty()
  ward: string;

  @ApiProperty()
  district: string;
}
