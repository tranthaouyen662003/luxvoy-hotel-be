import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { Room, RoomSchema } from './room.schema';

@Schema({ versionKey: false })
export class Hotel {
  @Prop()
  hotelName: string;

  @Prop()
  image: string[];

  @Prop()
  address: string;

  @Prop()
  rating: string;

  @Prop()
  lowestPrice: string;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  userId: mongoose.Types.ObjectId;

  @Prop({ type: [RoomSchema], default: [] }) // Use the HotelSchema to embed hotels
  rooms: Room[];
}

export const HotelSchema = SchemaFactory.createForClass(Hotel);
