import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';

@Schema({ versionKey: false })
export class Bill {
  @Prop()
  checkInDate: string;

  @Prop()
  checkOutDate: string;

  @Prop()
  paymentMethod: string;

  @Prop()
  rating: string;

  @Prop()
  comment: string;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Bill' })
  billId: mongoose.Types.ObjectId;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Room' })
  roomId: mongoose.Types.ObjectId;
}

export const BillSchema = SchemaFactory.createForClass(Bill);
