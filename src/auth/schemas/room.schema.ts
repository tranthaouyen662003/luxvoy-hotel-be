import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';

@Schema({ versionKey: false })
export class Room {
  @Prop()
  roomName: string;

  @Prop()
  type: string;

  @Prop()
  status: string;

  @Prop()
  price: string;

  @Prop()
  capacity: string;

  @Prop()
  roomImages: string[];

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Hotel' })
  hotelId: mongoose.Types.ObjectId;
}

export const RoomSchema = SchemaFactory.createForClass(Room);
