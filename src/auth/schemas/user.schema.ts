import { ObjectId } from 'mongodb';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { Hotel, HotelSchema } from './hotel.schema';

export enum RoleUser {
  USER = '0',
  ADMIN = '1',
}
export class UserAddress {
  @Prop()
  street: string;

  @Prop()
  ward: string;

  @Prop()
  district: string;
}
@Schema({ versionKey: false })
export class User {
  @Prop()
  username: string;

  @Prop()
  password: string;

  @Prop()
  fullName: string;

  @Prop()
  phoneNumber: string;

  @Prop()
  address: UserAddress;

  @Prop()
  role: RoleUser;

  @Prop({ type: [HotelSchema], default: [] }) // Use the HotelSchema to embed hotels
  hotels: Hotel[];
}

export const UserSchema = SchemaFactory.createForClass(User);
