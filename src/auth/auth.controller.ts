import { AuthService } from './auth.service';
import {
  Body,
  Controller,
  Get,
  Post,
  Query,
  Req,
  Session,
} from '@nestjs/common';
import { RegisterDto } from './dto/register.dto';
import { User } from './schemas/user.schema';
import { CreateHotelDto } from './dto/create-hotel.dto';
import { Hotel } from './schemas/hotel.schema';
import { dot } from 'node:test/reporters';
import { LoginDto } from './dto/login.dto';
import session from 'express-session';

@Controller('auth')
export class AuthController {
  constructor(private readonly AuthService: AuthService) {}

  @Post('register')
  async createUser(@Body() dto: RegisterDto): Promise<User> {
    const result = await this.AuthService.createUser(dto);
    return result;
  }

  @Get('users')
  async getUsers(@Query() query: object): Promise<User[]> {
    const users = await this.AuthService.getUsers(query);
    return users;
  }

  @Post('login')
  async login(@Body() dto: LoginDto): Promise<User> {
    const result = await this.AuthService.login(dto);
    return result;
  }

  @Post('hotels')
  async createHotel(@Body() dto: CreateHotelDto, @Req() request: any) {
    return this.AuthService.createHotel(dto);
  }

  @Get('get-hotels')
  async getHotels(@Query() query: object): Promise<Hotel[]> {
    const hotels = await this.AuthService.getHotels(query);
    return hotels;
  }
}
