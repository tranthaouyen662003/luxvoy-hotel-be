import {
  BadRequestException,
  Injectable,
  NotFoundException,
  Session,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import mongoose, { Model } from 'mongoose';
import { CreateHotelDto } from './dto/create-hotel.dto';
import { LoginDto } from './dto/login.dto';
import { RegisterDto } from './dto/register.dto';
import { Hotel } from './schemas/hotel.schema';
import { Room } from './schemas/room.schema';
import { User } from './schemas/user.schema';

@Injectable()
export class AuthService {
  constructor(
    @InjectModel('user')
    private readonly userModel: Model<User>,
    @InjectModel('hotels')
    private readonly hotelModel: Model<Hotel>,
    @InjectModel('rooms')
    private readonly roomModel: Model<Room>,
  ) {}

  ///AUTH
  async createUser(dto: RegisterDto): Promise<User> {
    const { fullName, phoneNumber, district, street, ward, password } = dto;

    const userExists = await this.userModel.exists({ phoneNumber }).exec();
    if (userExists) {
      throw new BadRequestException('PhoneNumber already exists!');
    }

    const newUser = new this.userModel({
      username: phoneNumber,
      password,
      fullName,
      phoneNumber,
      role: 0,
      street,
      ward,
      district,
    });

    await newUser.save();
    return newUser;
  }

  async login(dto: LoginDto): Promise<User> {
    const phoneNumber = dto.username;
    const userToAttempt = await this.userModel.findOne({
      phoneNumber,
    });

    if (userToAttempt == null) {
      throw new BadRequestException('Invalid phone number or password');
    }

    const isMatch = userToAttempt.password === dto.password;
    if (!isMatch)
      throw new BadRequestException('Invalid phone number or password');

    return userToAttempt;
  }

  /// MANAGEMENT
  async getUsers(query: object): Promise<User[]> {
    return this.userModel.find(query);
  }

  async createHotel(dto: CreateHotelDto): Promise<Hotel> {
    const { hotelName, image, rating, room, userId, lowestPrice } = dto;

    // Create an array to store the generated ObjectId references for rooms
    const rooms = [];

    // Iterate through the rooms and create and save each room individually
    for (const roomData of room) {
      const newRoom = new this.roomModel(roomData); // Assuming you have a RoomModel registered
      const savedRoom = await newRoom.save();
      rooms.push(savedRoom);
    }

    // Create the new hotel document using the generated roomObjectIds
    const newHotel = new this.hotelModel({
      hotelName,
      rating,
      image,
      userId,
      lowestPrice,
    });

    newHotel.rooms = rooms;
    await newHotel.save();

    // Fetch the user document
    const user = await this.userModel.findById(userId).exec();

    if (!user) {
      throw new NotFoundException('User not found');
    }

    // Update the user's hotels array with the new hotel's ID
    user.hotels.push(newHotel); // Convert the ObjectId to a string

    // Save the updated user document back to the database
    await user.save();

    return newHotel;
  }

  async getHotels(query: object): Promise<Hotel[]> {
    return this.hotelModel.find(query);
  }

  
}
